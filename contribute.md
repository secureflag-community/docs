# Contribution Guidelines

### Contributing to SecureFlag Platform Open Platform

We want to make contributing to SecureFlag as easy and transparent as possible, whether it's:

- Reporting a bug
- Discussing the current state of the code
- Submitting a fix
- Proposing new features

#### We Develop with GitLab

We use GitLab to host code, track issues and feature requests, and accept pull requests.

Pull requests are the best way to propose changes to the codebase:

1. Fork the repo and create your branch from `master`
2. If you've added code that should be tested, add tests
3. Issue the pull request

#### Report bugs using GitLab's Issues

We use GitLab Issues to track public bugs. Report a bug by opening a new issue; a good bug report should include:

- A quick summary and/or background
- Steps to reproduce
- What you expected would happen
- What actually happens
- Notes (possibly including why you think this might be happening or stuff you tried that didn't work)

#### License

When you submit code changes, your submissions are understood to be under the same [GNU GPL 3.0 license](http://choosealicense.com/licenses/gpl-3.0/) that covers the project. Feel free to contact the maintainers if that's a concern.
