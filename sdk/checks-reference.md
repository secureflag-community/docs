# Exercise Checks Reference

The YAML file `/SF/conf/flags.d/flags.yml` defines the majority of automated tests to check the status of a vulnerability and the state of the application.

Two further files are also used to define templates and further checks:

```shell
/SF/conf/actions.d/app.yml
/SF/conf/templates.d/templates.yml
```

### Vulnerability checks

The `flags` field in the YAML file defines the automated tests to check the status of a vulnerability.

It returns a string, like `vulnerable`, `fixed`, `broken-functionality`, or `broken-webserver`.

#### Flags

The `flags.yml` file may contain an arbitrary number of check objects; each check object uses a short name that suggests the name of the vulnerability and the affected component, e.g., `xss-index` .

Every check object must contain a list of steps. The steps are executed sequentially until one of them is evaluated as `true`.

#### Steps

Every step can be of a different type, e.g., `url` requests a URL, `cmd` executes a system command. Every step may contain an `assert` instruction that evaluates a line of Python code. If the `assert` instruction returns `true`, the sequential execution stops and returns the string in `status`.

The `onexit` instruction can be set to `true`, which will make a step run at exit to perform cleanup tasks.

The following section contains the step types:

##### **url**

Performs an HTTP request using Python Requests's  `requests.request()` [method](https://requests.readthedocs.io/en/master/api/#requests.request) and returns the `requests.Response` [object](https://requests.readthedocs.io/en/master/api/#requests.Response) as variable `res` to be evaluated in the `assert` section.  

Additional keys:

- `post_data`: dictionary with POST data passed to the Requests's `post` parameter.
- `post_json`: dictionary with JSON data passed to the Requests's `json` parameter.
- `headers`: dictionary with additional HTTP headers passed to the Requests's `headers` parameter.
- `post_files`: dictionary with files data passed to the `files` parameter.
- `allow_redirect`: boolean passed to the Python Requests's `allow_redirect` HTTP Request parameter.
- `method`: string passed to Python Requests's `method` parameter.

##### **cmd**

Executes a system command and returns the result in the variable `res` to be evaluated in the  `assert` section.

The result is a dictionary with the keys: `stdout`, `stderr`, and `retcode`.

##### **eval**

Executes code using Python's `exec()` [function](https://docs.python.org/2/reference/simple_stmts.html#exec).

##### **assert**

Evaluates code using Python's `eval` [function](https://docs.python.org/2/library/functions.html#eval). When it returns `true`, the sequential execution stops and returns the string in `status`. If it returns `false`, the next step is executed.

##### **drop_privileges**

Drop the privileges to the specified user so that the following steps run with reduced privileges. It's recommended to drop privileges as the first step.

#### Example

This is an example of a minimal `flags.yml` file that tests the presence of an XSS in the `/index` page:

```yaml
xss-index:
  - status: vulnerable
    url: http://localhost/index.php/<img src=x onerror=alert(1);>
    assert: |
      '<img src=x onerror=alert(1);>' in res.text
  - status: fixed
    url: http://localhost/index.php/<img src=x onerror=alert(1);>
    assert: |
      '&lt;img src=x onerror=alert(1);&gt;' in res.text
  - <<: *broken-functionality
```

### Action checks

The `actions.yml` file defines other tests related to the operating system status within the exercise's container.

Currently, it only supports the `app-status` check that should return a `running` or `stopped` status depending on whether the application server is currently running.

#### Example

This is an example of a health check of the application:

```yaml
app-status:
  - status: running
    url: http://localhost/
    assert: |
      res and res.status_code == 200
  - status: stopped
    assert: True
```

### Template File

The `templates.yml` file defines yaml templates that can be used by any of the other checks.

#### Example

This is an example of a health check of the application:

```yaml
templates:
  - &vulnerable
    status: vulnerable
    assert: true
  - &broken-functionality
    status: broken-functionality
    assert: true
  - &fixed
    status: fixed
    assert: true
```
