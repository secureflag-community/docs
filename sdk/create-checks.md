# Write Lab Checks

SecureFlag Platform provides the Automated Checker, an engine that tests, in an automated fashion, whether the user correctly remediated the vulnerability in the exercise.

The automated checker checks for vulnerabilities and other system metrics described by a combination `/SF/conf/actions.d/app.yml`, `/SF/conf/flags.d/flags.yml` and `/SF/conf/templates.d/templates.yml` YAML files inside the container.

> To read about the Automated Checker files structure, browse to the [Lab Checks Reference](/sdk/checks-reference.md).

### Vulnerability checks

The checks are designed to perform *real* tests on the functionality, such as injecting a payload to check if the vulnerability is still present. The same engine is also used to retrieve other system metrics, such as the application server status.

The `flags` field in the YAML file defines the automated tests to check the status of a vulnerability. The test of a single flag is defined by a list of steps. The steps are executed sequentially until one of them is evaluated as `true`. It returns a string like `vulnerable`, `fixed`, `broken-functionality`, or `broken-webserver`, that is automatically mapped to provide the user with feedback regarding the status of the exercise.

The checks for the sample Reflected XSS vulnerability in the HackMe application can be written as follows inside of `/SF/conf/flags.d/flags.yml`:

```yaml
xss-index:
  - status: vulnerable
    url: http://localhost/index.php/<img src=x onerror=alert(1);>
    assert: |
      '<img src=x onerror=alert(1);>' in res.text
  - status: fixed
    url: http://localhost/index.php/<img src=x onerror=alert(1);>
    assert: |
      '&lt;img src=x onerror=alert(1);&gt;' in res.text
  - <<: *broken-functionality
```

When the SecureFlag Platform queries the Automated Checker to verify what the vulnerability status of the exercise is, all the checks will execute sequentially until one is verified. The check returns `vulnerable` if the payload is reflected without being sanitized, `fixed` if HTML encoding has been applied correctly, or `broken-functionality` for any other unexpected scenario.

### Check binary

To help you develop the exercise checks, obtain a root shell on the development container and check the status by using the `/SF/bin/check` utility, like in the following example:

```shell
$ sfsdk img-shell org-php-hackme
root@org-php-hackme $ /SF/bin/check xss-index
{
  "errors": [], 
  "results": {
    "xss-index": "fixed"
  }
}
```

Adjust `/SF/conf/flags.d/flags.yml` until the command returns the expected results according to the vulnerability and application statuses.

Set the `DEBUG` environment variable to `1` to enable the debug prints.

Make sure to add the file `flags.yml` in the `fs/SF/conf/flags.d/` directory of your image before it gets published, along with any changes to `templates.yml` or `app.yml`.
