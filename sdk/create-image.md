# Create Lab Image

A SecureFlag Lab Image is a Docker image that contains an intentionally vulnerable application and all the development tools required by the trainee to run the virtualized Lab on the platform.

This guide provides step-by-step instructions on how to create a new SecureFlag image named `org-php-hackme`.

> You can find the resulting image of this training at the [org-php-hackme](https://gitlab.com/secureflag-community/org-php-hackme/) repository.

The image development lifecycle is the process of creating a new, custom SecureFlag image to host your vulnerable application and run SecureFlag exercises on. The picture below summarizes the development process.

![Development Lifecycle](../img/sf-dev-lifecycle.png)

## Initialize the New Image

Initialize an empty structure for a new image named `org-php-hackme`:

```console
$ sfsdk img-add org-php-hackme
[+] New image org-php-hackme has been added at '~/sf/img/org-php-hackme'
```

The command above creates minimal SecureFlag images' settings under `~/sf/images.yml` and initializes the build folder and the `Dockerfile` for your new image under `~/sf/img/org-php-hackme/`.

Use `sfsdk img-ls` to list the available images to see the image you just created.

```console
$ sfsdk img-ls
Name                      Running   Container name   RDP port   RDP creds                                                                                                    
--------------------------------------------------------------------------
org-php-hackme            No
```

## Build the Image

In the previous initialization, we created a minimal image to host your vulnerable application. The base structure of the build directory of the project is the following:

```shell
~/sf/img/org-php-hackme/
   ├── Dockerfile
   └── fs/
```

Try an initial build. This command results in `docker build` running on the build folder:

```console
$ sfsdk img-build org-php-hackme

[*] Building exercise image org-php-hackme...
Step 1/3 : FROM secureflag/sf-community

Step 2/3 : COPY fs/ /

Step 3/3 : RUN chown -R sf:sf /home/sf

Successfully tagged org-php-hackme:latest
[+] Build complete
```

As per the `Dockerfile`, any file in the `fs/` folder will be applied over the image's filesystem at build time, allowing any preferred customization.

## Run the Container for Development

Try to run the empty image by launching the last build with `sfsdk img-run`. This executes `docker run` using the images' settings stored in `~/sf/images.yml`.

```shell
$ sfsdk img-run org-php-hackme
[+] Container org-php-hackme-inst has been started

  Container name:     org-php-hackme-inst  
  RDP port:           127.0.0.1:3389       
  RDP credentials:    sf:password          
  Application port:   127.0.0.1:8050
```

Verify that the container is running with `sfsdk img-ls`:

```bash
$ sfsdk img-ls
Name                 Running Container name              RDP port   RDP creds                                                                      
--------------------------------------------------------------------------------
org-php-hackme       Yes     org-php-hackme-inst   3389       sf:password 
```

## Configure the Environment

You'll need to install and configure anything you need to reproduce a fully configured environment, such as an IDE, software packages, desktop and IDE configurations, etc., like in the following example.

### RDP into the desktop

The RDP address and credentials are displayed when the container starts or runs `sfsdk img-ls`. The default values are:

* RDP address: `127.0.0.1:3389`
* Username: `sf`
* Password: `password`

Use an RDP client of your choice (e.g., Remmina on Linux or Microsoft, Remote Desktop on macOS) to log into the running container. You should see the SecureFlag Desktop with the browser installed.

### Customize the container

Install any missing software, taking note of the changes you make to ensure reproducibility for the following Docker build.

For the purposes of this tutorial, we'll set up a minimal installation with Visual Studio Code IDE and a PHP web server that starts at boot.

Start by running `sfsdk img-shell` to obtain a root shell on the running container:

```console
$ sfsdk img-shell org-php-hackme

root:/#
```

Running the following commands (valid at the time this document was written) will install VSCode and PHP on the container:

```shell
curl -L 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -o /tmp/visualstudio.deb && \
  dpkg -i /tmp/visualstudio.deb && \
  rm -f /tmp/visualstudio.deb && \
  apt-get update && \
  apt-get -y install php-cli
```

You might also want to add a VSCode launcher on the Desktop by linking its application `.desktop` file.

```shell
ln -s /usr/share/applications/code.desktop /home/sf/Desktop/code.desktop
```

VSCode launcher requires an additional argument to run in a Docker container. Use your favorite terminal editor and modify `/usr/share/applications/code.desktop` by changing the `Exec` parameter as below:

```ini
Exec=/usr/share/code/code --no-sandbox --unity-launch %F
```

Observe that now, clicking the launcher on the Desktop will spawn a new VSCode window.

Finally, create an init file to run a PHP web server at the start of the container. Create a file with the following content at `/SF/init/enabled/10-php-server`, then make it executable with `chmod +x /SF/init/enabled/10-php-server`:

```bash
#!/bin/bash

php -t /home/sf/exercise/app -S 0.0.0.0:80 &
```

Try the web server without rebuilding the lab, create the web root with `mkdir -p /home/sf/exercise/app && chown sf:sf /home/sf/exercise/app` and run it manually by executing `/SF/init/enabled/10-php-server` from the root shell.

### Copy the customization to the build folder

Once you are satisfied with the setup in your container, you must reproduce the changes you made in the previous section into the Docker build context.

Replicate the command to install VSCode and PHP to `~/sf/img/org-php-hackme/Dockerfile` as a `RUN` command. Making sure to add this before the `COPY` command, so that the changes are applied to the image before the filesystem is copied over.

```docker
RUN curl -L 'https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64' -o /tmp/visualstudio.deb && \
  dpkg -i /tmp/visualstudio.deb && \
  rm -f /tmp/visualstudio.deb && \
  apt-get update && \
  apt-get -y install php-cli
```

Then, use `docker cp` to copy `code.desktop` and `10-php-server` files out of the container into the `fs` build directory.

```shell
cd ~/sf/img/org-php-hackme/
mkdir -p fs/usr/share/applications/ fs/home/sf/Desktop/
docker cp org-php-hackme-inst:/usr/share/applications/code.desktop fs/usr/share/applications/code.desktop
docker cp org-php-hackme-inst:/home/sf/Desktop/code.desktop fs/home/sf/Desktop/code.desktop # Ignore the warning here
```

```shell
cd ~/sf/img/org-php-hackme/
mkdir -p fs/SF/init/enabled/
docker cp org-php-hackme-inst:/SF/init/enabled/10-php-server fs/SF/init/enabled/10-php-server
```

Apply this approach to any customization you want to, copying the steps to your build folder to ensure reproducibility for the next build.

### Install the application

After configuring the environment, let's add the vulnerable application.

For the purpose of this tutorial, we're going to write a simple single-file PHP application called `hackme.php` that is vulnerable to Cross-Site Scripting. The application will be located within **/home/sf/exercise/app** in order to be run by the PHP server.

To install the example HackMe application, follow the steps from the RDP connection:

1. Switch to VSCode.

2. Click "File" > "Open Folder" and use the dialogs to create and open the new directory `~/exercise/app` in the home folder.

3. Expand the `app` directory on the left-side menu and right click it to add a new file named `index.php`

4. Write or copy the vulnerable code sample below in the `index.php` PHP script. The code below is vulnerable to Reflected Cross-Site Scripting:

   ```php
   <html>
   
     <h1>
       Hello! The time is <?php echo date("h:i:sa"); ?> 
     </h1>
   
     Click <a href="<?php echo($_SERVER['PHP_SELF']); ?>">here</a> 
     to reload the page.
   
   </html>
   ```

5. Reload the browser to make sure the PHP web server you started before is correctly running the PHP application:

   ![hackme](../img/sf-hackme.png)

6. Experiment with the vulnerable app to check everything works as expected.

7. Once you are satisfied with your changes, you can export the app in the build folder by using `docker cp`.

  ```shell
  cd ~/sf/img/org-php-hackme/
  mkdir -p fs/home/sf/exercise/
  docker cp org-php-hackme-inst:/home/sf/exercise/app fs/home/sf/exercise/app
  ```

## Export other changes

Saving changes of configuration files in the home directory might be tricky. To help with this, the SDK comes with `sfsdk img-watch`, which allows you to keep track of any changes you have made in the home folder during the environment customization process:

```shell
 $ sfsdk img-watch org-php-hackme

[+] Please wait, preparing sf-devops-inst to run img-watch..
[+] Started watching changes in /home/sf folder..
```

Keep this command running on a terminal while you configure, e.g., the IDE or the browser. For the sake of this tutorial, open VSCode via the `index.php` folder and close it. You'll notice a change has been detected:

```shell
[+] Changed files and folders:

  /home/sf/.config/
```

Once you have completed the customization, you can exit the `sfsdk img-watch` command with Ctrl-C.

Use this information with the `docker cp` command to copy the changed files from the running container you're working on to the build folder and then rebuild the image again:

```shell
docker cp org-php-hackme-inst:/home/sf/.config fs/home/sf/.config
```

Alternatively, you can use `sfsdk img-snapshot` to automatically create a new image as a snapshot. The new image is the copy of the current exercise build folder that includes the directories that were detected as having changed.

## Stop the Container

Stop the container, build and run the new image and check that it behaves as expected.

```bash
sfsdk img-stop org-php-hackme
```

## Repeat

Repeat the steps until the Desktop environment, the IDE, and the vulnerable application work as expected. During this process, you will be iterating through the following steps:

1. Build the image with `sfsdk img-build`.

2. Run the image with `sfsdk img-run`.

3. Install and set up the software in the container while updating the Dockerfile and `fs` directory accordingly with the changes you want to see in the next build.

4. Stop the old image by using `sfsdk img-stop`.

5. Build and restart the new version of the image.

When you have reached a satisfactory integration, add the checks in the following section.