# Intro to Labs Development

### Structure of a SecureFlag Lab

A SecureFlag Lab is made up of two components: the Metadata and the Image.

The Image is a custom Docker Image that extends a SecureFlag base image. The base image contains a minimal base line that can be extended to add an intentionally vulnerable application together with the automated checks to establish if a running Lab is fixed or still vulnerable.

The Metadata includes description, flags, points, duration, hints, solution, and other information related to the platform.

Lab Metadata and Image are bundled together in the distribution phase.

A single image can host multiple Labs, and each of the Lab is defined by its metadata. In this way, it is possible to create a vulnerable application with many security issues and create a different Lab for each vulnerability.

<p align="center"><img src="../img/sf-exercise-structure.png"/></p>

### Steps to Create a new SecureFlag Lab

The steps illustrated below show the flow to create a new SecureFlag lab in the image. Reference the rest of the SDK documentation to learn how to [create a Lab image](create-image.md) and add a vulnerable application with [checks](write-checks.md) to it.